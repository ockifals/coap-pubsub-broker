import datetime
import random
import logging
import asyncio
import json

import aiocoap.resource as resource
from aiocoap import Context, Message
from aiocoap.numbers.codes import Code


class TemperatureResource(resource.ObservableResource):

    def __init__(self):
        super().__init__()
        self.handle = None
        self.temp_observers = []
        self.queues = []
        self.queue = []

    @staticmethod
    def get_content():
        # TODO get from redis
        return {
            'topic_id': '1',
            'value': random.randint(15, 45),
            'timestamp': datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        }

    def notify(self):
        print('TEMP queues: ', end='')
        print(len(self.queues))

        # update observers count
        if len(self.temp_observers) != len(self._observations):
            self.temp_observers = []
            for i, observer in enumerate(self._observations):
                self.temp_observers.append(observer.original_request.token)

        if self.queues:
            self.updated_state()
            self.reschedule()
        else:
            self.reschedule()

    def reschedule(self):
        return asyncio.get_event_loop().call_later(5, self.notify)

    def update_observation_count(self, count):
        if count and self.handle is None:
            print("Serving temperature resource")
            self.handle = self.reschedule()
        if count == 0 and self.handle:
            print("Stopping temperature resource")
            self.handle.cancel()
            self.handle = None
            self.queues.insert(0, self.queue)

    async def render_get(self, request):
        if self.queues:
            data = self.queues[0]
            self.queue = data
            payload = data.get('data')

            if 1 == data.get('count'):
                self.queues.pop()
            else:
                self.queues[0]['count'] -= 1

        else:
            payload = json.dumps({'status': 'init'}).encode('ascii')
        return Message(payload=payload)

    async def render_post(self, request):
        print('POST payload: %s' % request.payload)
        print(len(self.temp_observers))
        self.queues.append({
            'data': request.payload,
            'count': len(self.temp_observers)  # observers count
        })
        res = json.dumps({'status': 'ok'}).encode('ascii')
        return Message(code=Code.CHANGED, payload=res)


class CarbonResource(resource.ObservableResource):

    def __init__(self):
        super().__init__()
        self.handle = None
        self.co_observers = []
        self.queues = []
        self.queue = []

    @staticmethod
    def get_content():
        # TODO get from redis
        return {
            'topic_id': '2',
            'value': random.randint(15, 45),
            'timestamp': datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        }

    def notify(self):
        print('Co queues: ', end='')
        print(len(self.queues))

        # update observers count
        if len(self.co_observers) != len(self._observations):
            self.co_observers = []
            for i, observer in enumerate(self._observations):
                self.co_observers.append(observer.original_request.token)

        if self.queues:
            self.updated_state()
            self.reschedule()
        else:
            self.reschedule()

    def reschedule(self):
        return asyncio.get_event_loop().call_later(5, self.notify)

    def update_observation_count(self, count):
        if count and self.handle is None:
            print("Serving carbon resource")
            self.handle = self.reschedule()
        if count == 0 and self.handle:
            print("Stopping carbon resource")
            self.handle.cancel()
            self.handle = None
            self.queues.insert(0, self.queue)

    async def render_get(self, request):
        if self.queues:
            data = self.queues[0]
            self.queue = data
            payload = data.get('data')

            if 1 == data.get('count'):
                self.queues.pop()
            else:
                self.queues[0]['count'] -= 1
        else:
            payload = json.dumps({'status': 'init'}).encode('ascii')
        return Message(payload=payload)

    async def render_post(self, request):
        print('POST payload: %s' % request.payload)
        print(len(self.co_observers))
        self.queues.append({
            'data': request.payload,
            'count': len(self.co_observers)  # observers count
        })
        res = json.dumps({'status': 'ok'}).encode('ascii')
        return Message(code=Code.CHANGED, payload=res)


# logging setup

# logging.basicConfig(level=logging.INFO)
# logging.getLogger("coap-server").setLevel(logging.DEBUG)


def main():
    root = resource.Site()

    root.add_resource(('.well-known', 'core'),
                      resource.WKCResource(root.get_resources_as_linkheader))
    root.add_resource(('temp',), TemperatureResource())
    root.add_resource(('co',), CarbonResource())

    asyncio.Task(Context.create_server_context(root))

    asyncio.get_event_loop().run_forever()


if __name__ == "__main__":
    main()
