import os
import sqlite3


class Db:
    def __init__(self):
        self.conn = None
        self.cursor = None

    def connect(self):
        is_new = False
        if not os.path.isfile("storage.db"):
            print('create new database storage.db')
            is_new = True
        self.conn = sqlite3.connect(r"storage.db")
        self.cursor = self.conn.cursor()
        if is_new:
            self.migrate()

    def disconnect(self):
        self.cursor.close()
        self.conn.close()

    # generate new tables
    def migrate(self):
        print('generate initial tables')
        sql_query_topics = "CREATE TABLE IF NOT EXISTS `topics` (" \
                           "`id`	INTEGER PRIMARY KEY AUTOINCREMENT," \
                           "`name` TEXT" \
                           ")"
        sql_query_sensordata_queues = "CREATE TABLE IF NOT EXISTS `sensordata_queues` (" \
                                      "`id`	INTEGER PRIMARY KEY AUTOINCREMENT," \
                                      "`topic_id`  INTEGER," \
                                      "`value`  TEXT," \
                                      "`timestamp`	TEXT)"
        self.cursor.execute(sql_query_topics)
        self.cursor.execute(sql_query_sensordata_queues)
        print('insert initial `topics` row')
        sql_query_topic = "INSERT INTO `topics` (`name`)" \
                          "VALUES ('temp'), ('co')"
        self.cursor.execute(sql_query_topic)
        self.conn.commit()
