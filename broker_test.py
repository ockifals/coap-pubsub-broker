#!/usr/bin/env python3

from aiocoap import Context, Message
from aiocoap.numbers.codes import Code
from random import randint
import asyncio
import json
import datetime

from random import randint


async def main():
    context = await Context.create_client_context()

    if 1 == randint(1, 2):
        print('Temp')
        res = {
            'topic_id': 1,
            'uri_path': 'temp'
        }
    else:
        print('Co')
        res = {
            'topic_id': 2,
            'uri_path': 'co'
        }

    data = {
        'topic_id': res['topic_id'],
        'value': randint(15, 45),
        'timestamp': datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    }
    payload = json.dumps(data).encode('ascii')
    request = Message(code=Code.POST, payload=payload)
    request.opt.uri_host = 'localhost'
    request.opt.uri_path = (res['uri_path'],)

    response = await context.request(request).response

    print('Result: %s\n%r'%(response.code, response.payload))

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
