# coap-pubsub-broker
An improved CoAP PubSub Broker

# Instruction

## Preparation

1. Set *__virtualenv__* path on __config.sh__

2. Install dependencies

```bash
$ ./setup.sh
```

## Run it!
*with lazy loader

- Run broker 

```bash
$ ./broker
```

- Run test 

```bash
$ ./broker_test
```
